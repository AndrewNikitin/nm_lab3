#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "elliptic.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(EllipticEquation &eq, QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_calc_button_clicked();

private:
    Ui::MainWindow *ui;
    EllipticEquation eq;
};
#endif // MAINWINDOW_H
