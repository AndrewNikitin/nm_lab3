#include "mainwindow.h"
#include "elliptic.h"

#include <QApplication>

void TestMethods(int x_steps, int y_steps)
{
    EllipticEquation eq(
                1.0, 1.0, 1.0, [](double x, double y){return -(x-y)*(2+x+y);},
                1.0, 1.0, [](double y){return -y*y;},
                1.0, 1.0, [](double y){return 100 + 20 - y*y;},
                1.0, 1.0, [](double x){return x*x;},
                1.0, 1.0, [](double x){return x*x - 100 - 20;},
                10.0, 10.0, [](double x, double y){return x*x - y*y;}
                );
    Solver solver;
    NumericSolution sol(solver.LibmanMethod(eq, x_steps, y_steps, 1e-12, 1.0));
    double hx = eq.l1 / x_steps;
    double hy = eq.l2 / y_steps;
    double max_error = 0.0;
    for (int i = 0; i <= x_steps; ++i)
    {
        for (int j = 0; j <= y_steps; ++j)
        {
            max_error = std::max(std::abs(eq.solution(hx*i, hy*j) - sol[i][j]), max_error);
            std::cout << sol[i][j] << ' ';
        }
        std::cout << std::endl;
    }
    std::cerr << max_error << '\n';
}

int main(int argc, char *argv[])
{
    EllipticEquation eq(
                1.0, 1.0, 1.0, [](double x, double y){return -(x-y)*(2+x+y);},
                1.0, 1.0, [](double y){return -y*y;},
                1.0, 1.0, [](double y){return 3 - y*y;},
                1.0, 1.0, [](double x){return x*x;},
                1.0, 1.0, [](double x){return x*x - 3;},
                1.0, 1.0, [](double x, double y){return x*x - y*y;}
                );
    QApplication a(argc, argv);
    MainWindow w(eq);
    w.show();
    return a.exec();
}
