#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(EllipticEquation &eq, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , eq(eq)
{
    ui->setupUi(this);

    ui->error_plot->addGraph();
//    ui->solution_plot->addGraph();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_calc_button_clicked()
{
    Solver solver;
    NumericSolution result;
    switch (ui->comboBox->currentIndex()) {
    case 0:
        result = solver.LibmanMethod(eq, ui->nx->value(), ui->ny->value(), 1e-12, 1.0);
        break;
    case 1:
        result = solver.SeidelMethod(eq, ui->nx->value(), ui->ny->value(), 1e-12, 1.0);
        break;
    case 2:
        result = solver.SeidelMethod(eq, ui->nx->value(), ui->ny->value(), 1e-12, 1.9);
        break;
    }
    double max_abs_err = 0.0;
    double hx = eq.l1 / ui->nx->value();
    double hy = eq.l2 / ui->ny->value();
    QVector<double> errors(ui->nx->value() + 1);
    QVector<double> x(ui->nx->value() + 1);
    for (int i = 0; i <= ui->nx->value(); ++i)
    {
        for(int j = 0; j <= ui->ny->value(); ++j)
        {
            max_abs_err = std::max(max_abs_err, std::abs(eq.solution(hx*i, hy*j) - result[i][j]));
            errors[i] = std::abs(eq.solution(hx*i, hy*j) - result[i][j]);
            x[i] = (hx * i);
        }
    }
    std::cerr << max_abs_err << std::endl;
    ui->error_plot->graph(0)->setData(x,errors);
    ui->error_plot->rescaleAxes();
    ui->error_plot->replot();

    ui->solution_plot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    ui->solution_plot->axisRect()->setupFullAxesBox(true);
    ui->solution_plot->xAxis->setLabel("y");
    ui->solution_plot->yAxis->setLabel("x");


    QCPColorMap *colorMap = new QCPColorMap(ui->solution_plot->xAxis, ui->solution_plot->yAxis);
    colorMap->data()->setSize(ui->nx->value() + 1, ui->ny->value() + 1);
    colorMap->data()->setRange(QCPRange(0, eq.l1), QCPRange(0, eq.l2));



    for(int i = 0; i <= ui->nx->value(); ++i) {
       for(int j = 0; j <= ui->ny->value(); ++j) {
          // std::cout << result[i][j] << ' ';
           colorMap->data()->setCell(j, i, result[i][j]);
       }
    }

    QCPColorScale *colorScale = new QCPColorScale(ui->solution_plot);
    ui->solution_plot->plotLayout()->addElement(0, 1, colorScale); // add it to the right of the main axis rect
    colorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
    colorMap->setColorScale(colorScale); // associate the color map with the color scale
    colorScale->axis()->setLabel("SOLUTION");

    colorMap->setGradient(QCPColorGradient::gpPolar);
    colorMap->rescaleDataRange();
    QCPMarginGroup *marginGroup = new QCPMarginGroup(ui->solution_plot);
    ui->solution_plot->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
    colorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);

    ui->solution_plot->rescaleAxes();
    ui->solution_plot->replot();
}
