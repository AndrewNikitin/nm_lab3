#include "elliptic.h"

bool StopCriteria(const NumericSolution &u_curr, NumericSolution &u_prev, int x_steps, int y_steps, double eps)
{
    double max_abs = 0.0;
    for (int i = 0; i <= x_steps; ++i)
    {
        for (int j = 0; j <= y_steps; ++j)
        {
            max_abs = std::max(max_abs, std::abs(u_curr[i][j] - u_prev[i][j]));
            u_prev[i][j] = u_curr[i][j];
        }
    }
    return max_abs < eps;
}

void InitialApproximation(const EllipticEquation &eq, NumericSolution &u, int x_steps, int y_steps)
{
//    double hx = eq.l1 / x_steps;
//    double hy = eq.l2 / y_steps;
    for (int i = 0; i <= x_steps; ++i)
    {
//        double y0 = 0;
//        double u0 = eq.gamma3(i * hx);
//        double y1 = eq.l2;
//        double u1 = eq.gamma4(i * hx);
//        for (int j = 0; j <= y_steps; ++j)
//        {
//            double y = j * hy;
//            double u_p = (y - y0) * (u1 - u0) / (y1 - y0) + u0;
//            u[i][j] = u_p;
//        }
        for(int j = 0; j <= y_steps; ++j)
            u[i][j] = 0.0;
    }
}

void ApproximateBorders(const EllipticEquation &eq, NumericSolution &u, int x_steps, int y_steps)
{
    double hx = eq.l1 / x_steps;
    double hy = eq.l2 / y_steps;

    for (int j = 0; j <= y_steps; ++j) {
        u[0][j] = (-eq.alpha1*u[1][j] + hx*eq.gamma1(hy*j)) / (eq.beta1*hx - eq.alpha1);
        u[x_steps][j] = (eq.alpha2*u[x_steps - 1][j] + hx*eq.gamma2(hy*j)) / (eq.beta2*hx + eq.alpha2);
    }
    for (int i = 0; i <= x_steps; ++i) {
         u[i][0] = (-eq.alpha3*u[i][1] + hy*eq.gamma3(hx*i))/(eq.beta3 * hy - eq.alpha3);
         u[i][y_steps] = (eq.alpha4*u[i][y_steps - 1] + hy*eq.gamma4(hx*i))/(eq.beta4 * hy + eq.alpha4);
    }
}

NumericSolution Solver::LibmanMethod(EllipticEquation &eq, int x_steps, int y_steps, double eps, double w)
{
    NumericSolution u_curr(x_steps + 1, std::vector<double>(y_steps + 1, 0));
    NumericSolution u_prev(x_steps + 1, std::vector<double>(y_steps + 1, 0));

    double hx = eq.l1 / x_steps;
    double hy = eq.l2 / y_steps;
    double hx2 = hx * hx;
    double hy2 = hy * hy;

    InitialApproximation(eq, u_prev, x_steps, y_steps);
    ApproximateBorders(eq, u_prev, x_steps, y_steps);

    for (int iter = 0; iter < MAX_ITERATIONS; ++iter)
    {
        for (int i = 1; i < x_steps; ++i)
        {
            for (int j = 1; j < y_steps; ++j)
            {
//            	double a1 = eq.a*hx*hy2*u_prev[i - 1][j] - eq.a*hx*hy2*u_prev[i + 1][j];
//            	double a2 = eq.b*hx2*hy*u_prev[i][j - 1] - eq.b*hx2*hy*u_prev[i][j + 1];
//                u_curr[i][j] = ( a1 + a2 +
//                                2*hx2*u_prev[i][j - 1] + 2*hx2*u_prev[i][j + 1] +
//                                2*hy2*u_prev[i - 1][j] + 2*hy2*u_prev[i + 1][j] -
//                                2*hx2*hy2*eq.f(i*hx, j*hy));
//                u_curr[i][j] /= (2*(eq.c*hx2*hy2 + 2*hx2 + 2*hy2));
//                u_curr[i][j] = w * u_curr[i][j] + (1 - w) * u_prev[i][j];
                double a1 = (1/hx2 + eq.a/(2*hx)) * u_prev[i-1][j];
                double a2 = (1/hy2 + eq.b/(2*hy)) * u_prev[i][j-1];
                double a3 = (-2/hx2 -2/hy2 -eq.c);
                double a4 = (1/hx2 - eq.a/(2*hx)) * u_prev[i+1][j];
                double a5 = (1/hy2 - eq.b/(2*hy)) * u_prev[i][j+1];
                double fij = eq.f(i*hx, j*hy);
                u_curr[i][j] = (-a1 -a2 -a4 -a5 + fij) / a3;
            }
        }
        ApproximateBorders(eq, u_curr, x_steps, y_steps);
        if (StopCriteria(u_curr, u_prev, x_steps, y_steps, eps))
        {
            std::cerr << "Evaluation complete. Iterations: " << iter + 1 << std::endl;
            return u_curr;
        }
    }
    std::cerr << "Evaluation complete. Reached max number of iterations" << std::endl;
    return u_curr;
}

NumericSolution Solver::SeidelMethod(EllipticEquation &eq, int x_steps, int y_steps, double eps, double w)
{
    NumericSolution u_curr(x_steps + 1, std::vector<double>(y_steps + 1, 0));
    NumericSolution u_prev(x_steps + 1, std::vector<double>(y_steps + 1, 0));

    double hx = eq.l1 / x_steps;
    double hy = eq.l2 / y_steps;
    double hx2 = hx * hx;
    double hy2 = hy * hy;

    InitialApproximation(eq, u_prev, x_steps, y_steps);
    ApproximateBorders(eq, u_prev, x_steps, y_steps);

    for (int iter = 0; iter < MAX_ITERATIONS; ++iter)
    {
        for (int i = 1; i < x_steps; ++i)
        {
            for (int j = 1; j < y_steps; ++j)
            {
                double a1 = eq.a*hx*hy2*u_curr[i - 1][j] - eq.a*hx*hy2*u_prev[i + 1][j];
                double a2 = eq.b*hx2*hy*u_curr[i][j - 1] - eq.b*hx2*hy*u_prev[i][j + 1];
                u_curr[i][j] = ( a1 + a2 +
                                2*hx2*u_curr[i][j - 1] + 2*hx2*u_prev[i][j + 1] +
                                2*hy2*u_curr[i - 1][j] + 2*hy2*u_prev[i + 1][j] -
                                2*hx2*hy2*eq.f(i*hx, j*hy));
                u_curr[i][j] /= (2*(eq.c*hx2*hy2 + 2*hx2 + 2*hy2));
                u_curr[i][j] = w * u_curr[i][j] + (1 - w) * u_prev[i][j];
            }
        }
        ApproximateBorders(eq, u_curr, x_steps, y_steps);
        if (StopCriteria(u_curr, u_prev, x_steps, y_steps, eps))
        {
            std::cerr << "Evaluation complete. Iterations: " << iter + 1 << std::endl;
            return u_curr;
        }
    }
    std::cerr << "Evaluation complete. Reached max number of iterations" << std::endl;
    return u_curr;
}
