#ifndef ELLIPTIC_H
#define ELLIPTIC_H

#include <iostream>
#include <functional>
#include <vector>
#include <cmath>

using NumericSolution = std::vector<std::vector<double>>;

class EllipticEquation
{
///
/// u_xx + u_yy = a * u_x + b * u_y + c * u + f
/// alpha1 * u_x(0, y) + beta1 * u(0, y) = gamma1(y)
/// alpha2 * u_x(l1, y) + beta2 * u(l1, y) = gamma2(y)
/// alpha3 * u_y(x, 0) + beta3 * u(x, 0) = gamma3(x)
/// alpha4 * u_x(x, l2) + beta4 * u(x, l2) = gamma4(x)   
///
public:
	double a, b, c;
	std::function<double(double, double)> f;

	double alpha1, beta1;
	std::function<double(double)> gamma1;

	double alpha2, beta2;
	std::function<double(double)> gamma2;

	double alpha3, beta3;
	std::function<double(double)> gamma3;

	double alpha4, beta4;
	std::function<double(double)> gamma4;

	double l1, l2;

	std::function<double(double, double)> solution;

	EllipticEquation(
			double a, double b, double c, std::function<double(double, double)> f,
			double alpha1, double beta1, std::function<double(double)> gamma1,
			double alpha2, double beta2, std::function<double(double)> gamma2,
			double alpha3, double beta3, std::function<double(double)> gamma3,
			double alpha4, double beta4, std::function<double(double)> gamma4,
			double l1, double l2, std::function<double(double, double)> solution
		)
	{
		this->a = a;
        this->b = b;
        this->c = c;
        this->f = f;

        this->alpha1 = alpha1;
        this->beta1 = beta1;
        this->gamma1 = gamma1;

        this->alpha2 = alpha2;
        this->beta2 = beta2;
        this->gamma2 = gamma2;

        this->alpha3 = alpha3;
        this->beta3 = beta3;
        this->gamma3 = gamma3;

        this->alpha4 = alpha4;
        this->beta4 = beta4;
        this->gamma4 = gamma4;

        this->l1 = l1;
        this->l2 = l2;
        this->solution = solution;
	}
};

class Solver
{
public:
	const static int MAX_ITERATIONS = 10000;
	// Solver
	NumericSolution LibmanMethod(EllipticEquation &eq, int x_steps, int y_steps, double eps = 1e-2, double w = 1.0);
    NumericSolution SeidelMethod(EllipticEquation &eq, int x_steps, int y_steps, double eps = 1e-4, double w = 1.0);
};
#endif
